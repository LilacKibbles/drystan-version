﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 direction;
    public float forwardSpeed = 12.0f;

    private int desiredLane = 1; //0:left , 1: middle, 2:right
    public float laneDistance = 2.5f;

    public float jumpForce = 10.0f;
    public float Gravity = -20.0f;

    public float hunger;
    public Text hungerLevel;
    public Slider hungerBar;

    public float score;
    public Text scoreBoard;
    public Text bonusScore;

    private bool foodStreak;
    private bool streakCheck;
    private int streakNum;
    public Text streakNo;
    private int streakBonus;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        hunger = 100;
        score = 0;
        foodStreak = false;
        streakCheck = false;
        InvokeRepeating("decreaseHunger", 2.0f, 0.25f);
        InvokeRepeating("scoreUp", 0, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerManager.isGameStarted)
            return;

        direction.z = forwardSpeed;

        //Move Player
        controller.Move(direction * Time.deltaTime);
        //Inputs for Jump
        if (controller.isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) || (SwipeManager.swipeUp))
            {
                Jump();
                hunger = hunger - 2.5f;
            }
        }
        else
        {
            direction.y += Gravity * Time.deltaTime;
        }
        //Inputs for lanes

        if (Input.GetKeyDown(KeyCode.RightArrow) || (SwipeManager.swipeRight))
        {
            desiredLane++;
            if (desiredLane == 3)
                desiredLane = 2;
            hunger = hunger - 1;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) || (SwipeManager.swipeLeft))
        {
            desiredLane--;
            if (desiredLane == -1)
                desiredLane = 0;
            hunger = hunger - 1;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || (SwipeManager.swipeDown))
        {
            hunger = hunger - 4;
            StartCoroutine(SlowDown());
        }

        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;

        if (desiredLane == 0)
        {
            targetPosition += Vector3.left * laneDistance;
            
        }
        else if (desiredLane == 2)
        {
            targetPosition += Vector3.right * laneDistance;
            
        }

        if (hunger > 50)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, 50 * Time.deltaTime);
        }
        else if (hunger <= 50 && hunger > 0)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, 25 * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, 10 * Time.deltaTime);
        }

        // update score text
        scoreBoard.text = score.ToString();

        // food streak text
        if (foodStreak == true)
        {
            streakNo.text = "FOOD STREAK: " + streakNum.ToString();
        }
        else
        {
            streakNo.text = "";
        }
        

        // update hunger bar
        hungerBar.value = hunger;

        // keep hunger level from exceeding 100
        if (hunger > 100)
        {
            hunger = 100;
        }

        // hunger level-based movement restrictions
        if (hunger <= 30 && hunger > 0)
        {
            jumpForce = 7.5f;
        }
        else if (hunger > 30)
        {
            jumpForce = 10.0f;
        }
        else
        {
            jumpForce = 0;
        }

        //transform.position = Vector3.Lerp(transform.position, targetPosition, 80 * Time.deltaTime);

        if (controller.transform.position.y < 0)
        {
            PlayerManager.gameOver = true;
        }
    }
    private void Jump()
    {
        direction.y = jumpForce;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit) {
        if (hit.transform.tag == "Obstacle")
        {
            PlayerManager.gameOver = true;
        }
    }

    IEnumerator SlowDown()
    {
        forwardSpeed = 4.0f;
        yield return new WaitForSeconds(0.3f);
        forwardSpeed = 12.0f;
    }

    IEnumerator FoodStreak()
    {
        streakCheck = true;
        foodStreak = true;
        yield return new WaitForSeconds(29.0f);
        streakBonus = streakNum * 20;
        score = score + streakBonus;
        streakNum = 0;
        streakCheck = false;
        foodStreak = false;
        StartCoroutine(AddBonus());
    }

    IEnumerator AddBonus()
    {
        
        bonusScore.text = "+" + streakBonus.ToString();
        yield return new WaitForSeconds(1.0f);
        bonusScore.text = "";
    }

    void decreaseHunger()
    {
        if (PlayerManager.isGameStarted == true)
        {
            if (hunger > 0)
            {
                hunger = hunger - 0.1f;
            }
        }
    }

    void scoreUp()
    {
        if (PlayerManager.isGameStarted == true)
        {
            if (foodStreak == true)
            {
                score = score + 2;
            }
            else
            {
                score = score + 1;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            if (hunger < 100)
            {
                hunger = hunger + 10;
                streakNum = streakNum + 1;
                
                if (streakCheck == false && streakNum >= 2)
                {
                    StartCoroutine(FoodStreak());
                }
            }
        }
        if (other.gameObject.tag == "Reticle")
        {
            PlayerManager.gameOver = true;
        }
        if (other.gameObject.tag == "Rocks")
        {
            PlayerManager.isRocks = true;
        }
        else if (other.gameObject.tag == "Dirt")
        {
            PlayerManager.isRocks = false;
        }
        
    }

}

