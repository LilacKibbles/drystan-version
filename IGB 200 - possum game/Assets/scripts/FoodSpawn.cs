﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawn : MonoBehaviour
{
    public GameObject food;

    public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("spawnFood", 5.0f, 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void spawnFood()
    {
        Instantiate(food, new Vector3(player.position.x, 0.5f, player.position.z + 20), player.rotation);
    }
    
}
