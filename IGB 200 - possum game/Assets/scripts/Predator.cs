﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Predator : MonoBehaviour
{
    // Position of camera
    public Transform camPos;

    // Target object
    public GameObject target;

    // Player position
    public Transform player;

    float swoopTime = 7.0f;
    float swoopPeriod = 17.0f;

    // Speed of bird
    float forwardSpeed = 20.0f;
    

    private bool isSwooping;
    private bool isReturning;
    //private bool isPreparing;
    private bool isDiving;

    // Start is called before the first frame update
    void Start()
    {
        // Position bird at camera position
        this.transform.position = new Vector3(camPos.transform.position.x, camPos.transform.position.y + 10, camPos.transform.position.z + 8);
        isSwooping = false;
        isReturning = false;
        //isPreparing = false;
        isDiving = false;
        InvokeRepeating("startSwoop", 7.0f, 17.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerManager.isGameStarted)
            return;


        float step = forwardSpeed * Time.deltaTime;

        if (isSwooping == false)
        {
            this.transform.position = new Vector3(camPos.transform.position.x, camPos.transform.position.y + 5, camPos.transform.position.z + 10);
        } 
        /*
        if (Time.time > swoopTime)
        {
            isSwooping = true;
            swoopTime += swoopPeriod;
            StartCoroutine(SwoopCycle());
        }
        */
        if (isReturning == true)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(camPos.transform.position.x - 20, camPos.transform.position.y + 20, camPos.transform.position.z + 50), step * 3);
        }
        /*
        if (isPreparing == true)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(GameObject.FindGameObjectWithTag("Reticle").transform.position.x, GameObject.FindGameObjectWithTag("Reticle").transform.position.y + 7, GameObject.FindGameObjectWithTag("Reticle").transform.position.z), step * 3);
        }
        */
        if (isDiving == true)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, GameObject.FindGameObjectWithTag("Reticle").transform.position, step * 6);
        }
        
    }

    void startSwoop()
    {
        if (PlayerManager.isGameStarted == true && PlayerManager.isRocks == false  && isSwooping == false && isReturning == false && isDiving == false)
        {
            isSwooping = true;
            swoopTime += swoopPeriod;
            StartCoroutine(SwoopCycle());
        }
    }
    /*
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && isSwooping == true)
        {
            PlayerManager.gameOver = true;
        }
    }
    */
    IEnumerator SwoopCycle()
    {
        Instantiate(target, new Vector3(player.position.x, 0.1f, player.position.z + 10), player.rotation);
        //isPreparing = true;
        yield return new WaitForSeconds(0.61f);
        isDiving = true;
        StartCoroutine(SwoopWait());
        
    }

    IEnumerator SwoopWait()
    {
        
        yield return new WaitForSeconds(0.1f);
        GameObject[] reticle = GameObject.FindGameObjectsWithTag("Reticle");
        foreach (GameObject target in reticle)
        {
            GameObject.Destroy(target);
        }
        isDiving = false;
        isReturning = true;
        StartCoroutine(SwoopEnd());
    }

    IEnumerator SwoopEnd()
    {
        
        yield return new WaitForSeconds(1.0f);
        isSwooping = false;
        isReturning = false;
    }
}
